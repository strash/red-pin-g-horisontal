extends Control


signal btn_info_back_pressed


enum BACK_BUTTON {
	BACK,
	SUB_BACK,
}

const TEXT: PoolStringArray = PoolStringArray([
	"In order not to lose all the money at once, they need to be divided into several equal parts. It is worth taking a certain amount for one game session. You cannot exceed the planned limit. Usually $ 5-10 is enough to play. This amount allows you to satisfy the feeling of excitement, try out several slots and beat the online casino.",
	"Most novice gamers never use the special offers of virtual gaming halls. As a result, they have to spend more of their own money on bets. In online casinos, bonuses can be obtained for registration, for making a deposit, for activity. There are also a number of incentive bonuses (for example, for a birthday). They can be in the form of a cash amount on the account or a series of free spins on slots.",
	"Choosing the first available slot and chaotic bets rarely bring success. For a deliberate game, you should focus on one of the working strategies that are based on the theory of probability. Although they do not give a 100% guarantee of winning, they allow you to systematize bets and control the expense of funds"
])

const TWEEN_SPEED: float = 0.2
var active_text_info: int = 1


# BUILTINS - - - - - - - - -


func _ready() -> void:
	change_back_buttons(BACK_BUTTON.BACK)


# METHODS - - - - - - - - -


func show_text(state: bool) -> void:
	var _t: bool
	($TextInfo as Label).text = TEXT[active_text_info - 1]
	var char_count: float = ($TextInfo as Label).get_total_character_count() / 20.0
	if state:
		_t = ($Tween as Tween).interpolate_property($TextInfo as Label, "percent_visible", null, 1.0, TWEEN_SPEED * char_count)
	else:
		if ($Tween as Tween).is_active():
			_t = ($Tween as Tween).stop($TextInfo as Label, "percent_visible")
		_t = ($Tween as Tween).interpolate_property($TextInfo as Label, "percent_visible", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func show_button_list(state: bool) -> void:
	var _t: bool
	if state:
		($Grid as BoxContainer).show()
		_t = ($Tween as Tween).interpolate_property($Grid as BoxContainer, "modulate:a", null, 1.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		_t = ($Tween as Tween).interpolate_property($Grid as BoxContainer, "modulate:a", null, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
		($Grid as BoxContainer).hide()



func change_back_buttons(show_button: int) -> void:
	var _t: bool
	if show_button == BACK_BUTTON.BACK:
		($Header/BtnBack as Button).show()
		_t = ($Tween as Tween).interpolate_property($Header/BtnBackSub as Button, "modulate:a", null, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
		($Header/BtnBackSub as Button).hide()
	elif show_button == BACK_BUTTON.SUB_BACK:
		($Header/BtnBackSub as Button).show()
		_t = ($Tween as Tween).interpolate_property($Header/BtnBackSub as Button, "modulate:a", null, 1.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
		($Header/BtnBack as Button).hide()


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_info_back_pressed")


func _on_BtnBackSub_pressed() -> void:
	change_back_buttons(BACK_BUTTON.BACK)
	show_text(false)
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	show_button_list(true)


func _on_BtnInfo1_pressed() -> void:
	change_back_buttons(BACK_BUTTON.SUB_BACK)
	active_text_info = 1
	show_button_list(false)
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	show_text(true)


func _on_BtnInfo2_pressed() -> void:
	change_back_buttons(BACK_BUTTON.SUB_BACK)
	active_text_info = 2
	show_button_list(false)
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	show_text(true)


func _on_BtnInfo3_pressed() -> void:
	change_back_buttons(BACK_BUTTON.SUB_BACK)
	active_text_info = 3
	show_button_list(false)
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	show_text(true)