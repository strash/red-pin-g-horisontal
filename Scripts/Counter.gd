extends Label


var increase: bool = false
var count: float = 0.0

var green: Color = Color(242.0 / 255.0, 153.0 / 255.0, 74.0 / 255.0)
var red: Color = Color(164.0 / 255.0, 2.0 / 255.0, 201.0 / 255.0)
var score_lifetime: float = 2.6

var REGEX: RegEx = RegEx.new()


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _r: int = REGEX.compile("(\\d)(?=(\\d\\d\\d)+([^\\d]|$))")
	var pos: Vector2 = rect_position
	var new_pos: Vector2
	var new_pos_x: float = rand_range(pos.x - 20.0, pos.x + 20.0)
	if increase:
		self.text = "$ +%s" % format_number(count)
		self.set("custom_colors/font_color", green)
		new_pos = Vector2(new_pos_x, rand_range(pos.y - 150.0, pos.y - 100.0))
		($CPUParticles2D as CPUParticles2D).amount = int(count / 3.0 if count < 1000.0 else 1000.0)
		($CPUParticles2D as CPUParticles2D).emitting = true
	else:
		self.text = "$ -%s" % format_number(count)
		self.set("custom_colors/font_color", red)
		new_pos = Vector2(new_pos_x, rand_range(pos.y + get_viewport_rect().size.x / 10.0, pos.y + get_viewport_rect().size.y / 10.0))
	var _t: bool = ($Tween as Tween).interpolate_property(self, "rect_position", pos, new_pos, score_lifetime, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(score_lifetime), "timeout")
	_t = ($Tween as Tween).interpolate_property(self, "modulate:a", 1.0, 0.0, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(0.3), "timeout")
	queue_free()


# METHODS - - - - - - - - -


# форматирование числа, проставление знаков между порядками
func format_number(number: float) -> String:
	var formated_number: String = REGEX.sub(str(number), "$1,", true)
	return formated_number


# SIGNALS - - - - - - - - -
