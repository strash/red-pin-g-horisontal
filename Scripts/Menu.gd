extends Control


signal btn_slots_pressed
signal btn_info_pressed


func _on_BtnSlots_pressed() -> void:
	emit_signal("btn_slots_pressed")


func _on_BtnInfo_pressed() -> void:
	emit_signal("btn_info_pressed")